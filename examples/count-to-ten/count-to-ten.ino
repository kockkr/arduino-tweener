#include <kok_Tween.h>
#include <kok_Tweener.h>

using namespace kok;

// Define a tweener object that repeats indefinitely
// - A negative integer provided to the tweener means to repeat indefinitely
// - A positive integer means repeat that number of times
Tweener tweener = Tweener(-1);

void setup() {
  // start a serial connection
  Serial.begin(9600);

  // delay for three seconds to ensure the serial connection has been made
  delay(3000);

  // add a tween to the tweener object that goes from 0 to 10 over a 5 second period
  tweener.push(Tween(1, 10, 5000));
  // ... followed by a tween that counts backwards from 10 to 1 over a 3 second period
  tweener.push(Tween(10, 1, 3000));

  // start the tween
  tweener.start();
}

void loop() {
  // print the number to the serial console
  Serial.println(tweener.step());
}
