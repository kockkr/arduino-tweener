/*
 * Tweening example for NeoPixel LED strips.
 * 
 * Tween() and TweenList() are used together to make complicated time-based animations.
 * 
 * A Tween() object specifies a single change between two values. Tween() takes
 * at least three arguments, a start value, a stop value, and a time duration in milliseconds.
 * 
 * TweenList() chains together Tween() objects to create sequences of tweens
 */
#define NUM_PIXELS 8
#define PIN_OUTPUT 10

#include <Adafruit_NeoPixel.h>
#include "kok_Tweener.h"
#include "kok_Tween.h"

// setup the neopixels
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_PIXELS, PIN_OUTPUT, NEO_GRB + NEO_KHZ800);

using namespace kok;

// TweenList(number of iterations) values < 0 repeat indefinitely
Tweener tweensRed1 = Tweener(-1);
Tweener tweensGreen1 = Tweener(-1);
Tweener tweensBlue1 = Tweener(-1);

Tweener tweensRed2 = Tweener(-1);
Tweener tweensGreen2 = Tweener(-1);
Tweener tweensBlue2 = Tweener(-1);

Tweener tweensPos1 = Tweener(-1);
Tweener tweensPos2 = Tweener(-1);

void setup() {
  // setup color transitions
  // Tween(start value, stop value, duration)

  // green for three seconds
  tweensRed1.push(Tween(10, 10, 1000));
  tweensGreen1.push(Tween(40, 40, 1000));
  tweensBlue1.push(Tween(0, 0, 1000));

  // cyan for three seconds
  tweensRed1.push(Tween(0, 0, 1000));
  tweensGreen1.push(Tween(20, 20, 1000));
  tweensBlue1.push(Tween(40, 40, 1000));

  // purple for three seconds
  tweensRed1.push(Tween(30, 30, 1000));
  tweensGreen1.push(Tween(0, 0, 1000));
  tweensBlue1.push(Tween(50, 50, 1000));


  // cyan for three seconds
  tweensRed2.push(Tween(0, 0, 1000));
  tweensGreen2.push(Tween(20, 20, 1000));
  tweensBlue2.push(Tween(40, 40, 1000));

  // purple for three seconds
  tweensRed2.push(Tween(30, 30, 1000));
  tweensGreen2.push(Tween(0, 0, 1000));
  tweensBlue2.push(Tween(50, 50, 1000));

  // green for three seconds
  tweensRed2.push(Tween(10, 10, 1000));
  tweensGreen2.push(Tween(40, 40, 1000));
  tweensBlue2.push(Tween(0, 0, 1000));

  // tween position in the strip
  // Tween(startValue, stopValue, duration, easing, delay, iterations, direction)
  tweensPos1.push(Tween(-1, NUM_PIXELS, 1000, Tween::Ease::Linear, 0, 1, Tween::Direction::forward));
  tweensPos2.push(Tween(-1, NUM_PIXELS, 1000, Tween::Ease::Linear, 0, 1, Tween::Direction::reverse));

  // start all tweens
  tweensRed1.start();
  tweensGreen1.start();
  tweensBlue1.start();

  tweensRed2.start();
  tweensGreen2.start();
  tweensBlue2.start();

  tweensPos1.start();
  tweensPos2.start();

  strip.begin();
}

void loop() {
  double red1, green1, blue1;
  double red2, green2, blue2;
  double pos1, pos2;

  // fetch tweening values at current time
  red1 = tweensRed1.step();
  green1 = tweensGreen1.step();
  blue1 = tweensBlue1.step();

  red2 = tweensRed2.step();
  green2 = tweensGreen2.step();
  blue2 = tweensBlue2.step();

  pos1 = tweensPos1.step();
  pos2 = tweensPos2.step();

   // light up the pixels
  clear();
  render(strip, pos1, red1, green1, blue1);
  render(strip, pos2, red2, green2, blue2);
}

void clear() {
  for (uint8_t i = 0; i < NUM_PIXELS; i++) {
    strip.setPixelColor(i, 0, 0, 0);
  }
}

void render(Adafruit_NeoPixel &strip, double pos, uint8_t red, uint8_t green, uint8_t blue) {
  int32_t basePos;
  double pxFract1, pxFract2;

  // calculate how to split illumination levels between the two pixels adjacent the specified position
  // for an anti-aliasing like effect.
  // - example: if position is 8.6, pixel 8 should be lit @ 40%, and pixel 9 @ 60%.
  basePos = floor(pos);
  pxFract2 = pos - basePos;
  pxFract1 = 1 - pxFract2;

  // set active pixels
  strip.setPixelColor(basePos, red * pxFract1, green * pxFract1, blue * pxFract1);
  strip.setPixelColor(basePos + 1, red * pxFract2, green * pxFract2, blue * pxFract2);

  strip.show();
}