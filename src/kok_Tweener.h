#ifndef kok_Tweener_h
#define kok_Tweener_h

#include "Arduino.h"
#include "linked_list/kok_LinkedList.h"
#include "kok_Tween.h"

namespace kok {

  class Tweener {

    private:
      LinkedList<Tween> m_queue;
      Tween m_currentTween;
      int32_t m_iterations;
      int32_t m_curIteration = 1;
      bool m_isPaused = false;
      uint32_t m_pauseTime;

    public:
      Tweener(int32_t iterations = 1);
      void start();
      void pause();
      double step();
      double step(double progress);
      uint32_t push(Tween tween);
      uint32_t unshift(Tween tween);
//    uint32_t insertBefore(uint32_t id, Tween tween);
      Tween pop();
      Tween shift();
//    Tween remove(uint32_t id);
  };

};

#endif
