#ifndef kok_LinkedList_h
#define kok_LinkedList_h

#include "Arduino.h"

namespace kok {

  template <typename ListValue>
  class LinkedList {
    private:
      uint32_t _length = 0;

      struct _ListItem {
        _ListItem* prev;
        _ListItem* next;
        ListValue value;
      };
      _ListItem* _first;
      _ListItem* _last;
      _ListItem* _currentItem;

    public:
      LinkedList();
      uint32_t length();

      // traversing methods
      ListValue prev();
      ListValue next();
      ListValue first();
      ListValue last();
      ListValue current();
      ListValue find(uint32_t id);

      // adding methods
      uint32_t push(ListValue value);    // add to end
      uint32_t unshift(ListValue value); // add to beginning
//    uint32_t insertBefore(uint32_t id, ListValue value);

      // removing methods
      ListValue pop();                   // remove from end
      ListValue shift();                 // remove from beginning
      ListValue remove(uint32_t id);     // remove from specified index

  };

  template <typename ListValue>
  LinkedList<ListValue>::LinkedList() {}

  template <typename ListValue>
  uint32_t LinkedList<ListValue>::length() {
    return _length;
  }

  template <typename ListValue>
  ListValue LinkedList<ListValue>::prev() {
    _currentItem = _currentItem->prev;
    return _currentItem->value;
  }

  template <typename ListValue>
  ListValue LinkedList<ListValue>::next() {
    _currentItem = _currentItem->next;
    return _currentItem->value;
  }

  template <typename ListValue>
  ListValue LinkedList<ListValue>::first() {
    _currentItem = _first;
    return _currentItem->value;
  }

  template <typename ListValue>
  ListValue LinkedList<ListValue>::last() {
    _currentItem = _last;
    return _currentItem->value;
  }

  template <typename ListValue>
  ListValue LinkedList<ListValue>::current() {
    return _currentItem->value;
  }

  /*
   * Find the item at the index specified
   */
  template <typename ListValue>
  ListValue LinkedList<ListValue>::find(uint32_t id) {
    _ListItem* findItem;
    int32_t pos = 0;

    // convert any out-of-bounds indexes to their corresponding index
    if (id != 0) {
      pos = id % _length;
      if (id < 0) {
        pos = _length - pos;
      }
    }

    // find the item in the list
    // - search from the beginning or the end depending on if the requested item is in the front or back half of the list
    if (pos > (_length / 2)) {
      // search items backwards starting from the end
      findItem = _last;
      for (uint32_t i = _length - 1; i > pos; i--) {
        findItem = findItem->prev;
      }
    } else {
      // search items forwards starting from the beginning
      findItem = _first;
      for (uint32_t i = 0; i < pos; i++) {
        findItem = findItem->next;
      }
    }

    return findItem->value;
  }

  /*
   * Add value to end of list
   */
  template <typename ListValue>
  uint32_t LinkedList<ListValue>::push(ListValue value) {
    _ListItem* item = new _ListItem();

    item->value = value;

    if (_length == 0) {
      item->prev = item;
      item->next = item;
      _first = item;
      _last = item;
      _currentItem = item;
    } else {
      item->prev = _last;
      item->next = _first;
      _last->next = item;
      _first->prev = item;

      _last = item;
    }

    _length++;

    return _length;
  }

  /*
   * Add value to beginning of list
   */
  template <typename ListValue>
  uint32_t LinkedList<ListValue>::unshift(ListValue value) {
    _ListItem* item = new _ListItem();

    item->value = value;

    if (_length == 0) {
      item->prev = item;
      item->next = item;
      _first = item;
      _last = item;
      _currentItem = item;
    } else {
      item->prev = _last;
      item->next = _first;
      _last->next = item;
      _first->prev = item;

      _first = item;
    }

    _length++;

    return _length;
  }
/*
  template <typename ListValue>
  uint32_t LinkedList<ListValue>::insertBefore(uint32_t id, ListValue value) {
    _ListItem* prevItem;
    _ListItem* newItem = new _ListItem;
    int32_t pos = 0;

    if (id != 0) {
      pos = id % _length;

      // if a negative index was provided
      if (id < 0) {
        pos = _length - pos;    // modulus always returns positive values - subtract value from length to get proper index
      }
    }

    // find the element at the specified index
    prevItem = _first;
    for (int32_t i = 0; i < pos; i++) {
      prevItem = prevItem->next;
    }

    // insert new item
//    newItem = _ListItem();
    newItem->value = value;
    newItem->prev = prevItem;
    newItem->next = prevItem->next;

    // modify existing items to point to new item
    newItem->prev->next = newItem;
    newItem->next->prev = newItem;

    _length++;

    return _length;
  }
*/

  /*
   * Remove value from end of list
   */
  template <typename ListValue>
  ListValue LinkedList<ListValue>::pop() {
    _ListItem deletedItem;

    deletedItem = *_last;

    _last->prev->next = _first;
    _first->prev = _last->prev;
    
    delete _last;

    _last = _first->prev;

    return deletedItem.value;
  }

  /*
   * Remove value from beginning of list
   */
  template <typename ListValue>
  ListValue LinkedList<ListValue>::shift() {
    _ListItem deletedItem;

    deletedItem = *_first;

    _first->next->prev = _last;
    _last->next = _first->next;

    delete _first;

    _first = _last->next;

    return deletedItem.value;
  }

/*
  template <typename ListValue>
  ListValue LinkedList<ListValue>::remove(uint32_t id) {
    _ListItem* removeItem;
    _ListItem deletedItem;
    int32_t pos = 0;

    // convert any out-of-bounds indexes to their corresponding index
    if (id != 0) {
      pos = id % _length;
      if (id < 0) {
        pos = _length - pos;
      }
    }

    // find the element at the specified index
    removeItem = _first;
    for (int32_t i = 0; i < pos; i++) {
      removeItem = removeItem->next;
    }

    // delete the item
    removeItem->prev->next = removeItem->next;
    removeItem->next->prev = removeItem->prev;

    deletedItem = *removeItem;

    delete removeItem;

    return deletedItem;
  }
*/

}; // end namespace kok

#endif // end define kok_LinkedList_h
