#include "kok_Tween.h"

using namespace kok;
Tween::Tween(double startValue, double stopValue, uint32_t duration, Ease ease, int32_t delay, uint8_t iterations, Direction direction) {
  static uint8_t uid = 0;

  m_uid = uid;
  uid++;

  m_options.duration = duration;
  m_options.startValue = startValue;
  m_options.stopValue = stopValue;
  m_options.ease = ease;
  m_options.delay = delay;
  m_options.iterations = iterations;
  m_options.direction = direction;

  m_startTime = millis();
};

uint32_t Tween::uid() {
  return m_uid;
}

void Tween::start(uint32_t startTime) {
  m_startTime = (startTime == NULL) ? millis() : startTime;
  m_stopTime = m_startTime + m_options.delay + (m_options.duration * m_options.iterations);
  m_isComplete = false;
/*
  if (m_uid == 0 || m_uid == 1 || m_uid == 2) {
    Serial.print("UID: ");
    Serial.println(m_uid);
    Serial.print("START TIME: ");
    Serial.println(m_startTime);
    Serial.print("STOP TIME: ");
    Serial.println(m_stopTime);
    Serial.println("");
  }
*/
}

/*
void Tween::reset() {
  m_startTime = millis();
  m_isComplete = false;
}
*/

// TODO: Tween::restart()
// - set startTime = millis() - (elapsed time past duration)
// m_startTime = millis() - (millis() - (m_startTime + duration));
/*
void Tween::restart() {
  // set start time to accomodate drift from previous tween
  m_startTime = millis() - (millis() - (m_startTime + m_options.duration));
  m_isComplete = false;
}
*/


/*
double Tween::step() {
  double progress = 0;

  if (m_isComplete) {
    progress = 1;
    m_currentIteration = m_options.iterations;
  } else {
    int32_t elapsedTime;
    uint32_t iteration;

    // calculate elapsed time
    elapsedTime = millis() - (m_startTime + m_options.delay);
    if (elapsedTime < 0) {
      m_currentIteration = 1;
      progress = 0;
    } else {
      // calculate the iteration
      // ceil() returning same as floor() ???
//    iteration = ceil(elapsedTime / _options.duration);
      iteration = floor(elapsedTime / m_options.duration) + 1;

      // calculate the elapsed time for just the current iteration
      elapsedTime = elapsedTime % m_options.duration;

      // calculate the step progress
      if (m_options.iterations >= 0 && iteration > m_options.iterations) {
        m_isComplete = true;
        progress = 1;
      } else {
        progress = elapsedTime / (float) m_options.duration;
      }

      // set progress to 0 on iteration change
      // - iteration change may not happen exactly in sync with progress calculation because of float errors
      //   and can occasionally cause a 'blip' on alternating tweens where progress is briefly set to 1 instead of 0.
      // - moving this block before the elapsedTime and step progress calculations and making them conditional, since
      //   this block overwrites their results, doesn't seem to fix the 'blip' issue. No ideas why.
      if (iteration != m_currentIteration) {
        progress = 0;
        m_currentIteration = iteration;
      }
    }
  }

  // invert progress for reverse directions
  if (m_options.direction == Direction::reverse
  || (m_options.direction == Direction::alternate && (m_currentIteration + 1) % 2)
  || (m_options.direction == Direction::alternateReverse && (m_currentIteration % 2) )) { 
    progress = 1 - progress;
  }

  return Tween::interpolate(progress);
};
*/
/*
 * 
 * if reverse
 *   elapsedTime = duration - elapsedTime;
 *   
 *   return Tween::interpolate(elapsedTime);
 *   - beginning is set in m_options
 *   - change can be calculated from stopValue - startValue 
 *   - duration is set in m_options
 * 
 * */

double Tween::step() {
  int32_t elapsedTime;

  // calculate elapsed time, current iteration, and if tween is complete
  if (m_isComplete) {
    elapsedTime = m_options.duration;
    m_currentIteration = m_options.iterations;
  } else {
    uint8_t iteration;

    // calculate elapsed time
    elapsedTime = millis() - (m_startTime + m_options.delay);

    if (elapsedTime < 0) {
      elapsedTime = 0;
      m_currentIteration = 1;
    } else {
      // calculate the iteration
      // ceil() returning same as floor() ???
//    iteration = ceil(elapsedTime / _options.duration);
      iteration = floor(elapsedTime / m_options.duration) + 1;

      // calculate the elapsed time for just the current iteration
      elapsedTime = elapsedTime % m_options.duration;
/*
      if (m_options.iterations >= 0 && iteration > m_options.iterations) {
        m_isComplete = true;
        elapsedTime = m_options.duration;
      }
*/
      if (iteration > m_options.iterations) {
        m_isComplete = true;
        elapsedTime = m_options.duration;
      }

      if (iteration != m_currentIteration) {
        elapsedTime = 0;
        m_currentIteration = iteration;
      }
    }
  }

  // invert progress for reverse directions
  if (m_options.direction == Direction::reverse
  || (m_options.direction == Direction::alternate && (m_currentIteration + 1) % 2)
  || (m_options.direction == Direction::alternateReverse && (m_currentIteration % 2) )) { 
    elapsedTime = m_options.duration - elapsedTime;
  }

  // interpolate
  return Tween::interpolate(elapsedTime, m_options.startValue, (m_options.stopValue - m_options.startValue), m_options.duration);
}

double Tween::step(double progress) {
  uint32_t elapsedTime;
  uint32_t iteration;

  // calculate the iteration for the progress value supplied
  iteration = floor(abs(progress)) + 1;

  // calculate progress (if progress not between 0 and 1)
  progress = abs(progress) - floor(abs(progress));

  // TODO: Convert progress to time | duration

//  return Tween::interpolate(progress);

  // progress = elapsedTime / duration
  elapsedTime = progress * m_options.duration;

  // invert progress for reverse directions
  if (m_options.direction == Direction::reverse
  || (m_options.direction == Direction::alternate && (iteration + 1) % 2)
  || (m_options.direction == Direction::alternateReverse && (iteration % 2) )) { 
    elapsedTime = m_options.duration - elapsedTime;
  }

  return Tween::interpolate(elapsedTime, m_options.startValue, (m_options.stopValue - m_options.startValue), m_options.duration);
}

/*
double Tween::step(double progress) {
  uint32_t iteration;

  // calculate the iteration for the progress value supplied
  iteration = floor(abs(progress)) + 1;

  // calculate progress (if progress not between 0 and 1)
  progress = abs(progress) - floor(abs(progress));

  return Tween::interpolate(progress);
};
*/

double Tween::interpolate(double time, double beginning, double change, double duration) {
  double stepValue;

  switch(m_options.ease) {
  case Ease::QuadEaseIn:
    stepValue = Easing::Quad::easeIn(time, beginning, change, duration);
    break;
  case Ease::QuadEaseOut:
    stepValue = Easing::Quad::easeOut(time, beginning, change, duration);
    break;
  case Ease::QuadEaseInOut:
    stepValue = Easing::Quad::easeInOut(time, beginning, change, duration);
    break;
  case Ease::QuartEaseIn:
    stepValue = Easing::Quart::easeIn(time, beginning, change, duration);
    break;
  case Ease::QuartEaseOut:
    stepValue = Easing::Quart::easeOut(time, beginning, change, duration);
    break;
  case Ease::QuartEaseInOut:
    stepValue = Easing::Quart::easeInOut(time, beginning, change, duration);
    break;
  case Ease::QuintEaseIn:
    stepValue = Easing::Quint::easeIn(time, beginning, change, duration);
    break;
  case Ease::QuintEaseOut:
    stepValue = Easing::Quint::easeOut(time, beginning, change, duration);
    break;
  case Ease::QuintEaseInOut:
    stepValue = Easing::Quint::easeInOut(time, beginning, change, duration);
    break;
  case Ease::SineEaseIn:
    stepValue = Easing::Sine::easeIn(time, beginning, change, duration);
    break;
  case Ease::SineEaseOut:
    stepValue = Easing::Sine::easeOut(time, beginning, change, duration);
    break;
  case Ease::SineEaseInOut:
    stepValue = Easing::Sine::easeInOut(time, beginning, change, duration);
    break;
  case Ease::ExpoEaseIn:
    stepValue = Easing::Expo::easeIn(time, beginning, change, duration);
    break;
  case Ease::ExpoEaseOut:
    stepValue = Easing::Expo::easeOut(time, beginning, change, duration);
    break;
  case Ease::ExpoEaseInOut:
    stepValue = Easing::Expo::easeInOut(time, beginning, change, duration);
    break;
  case Ease::ElasticEaseIn:
    stepValue = Easing::Elastic::easeIn(time, beginning, change, duration);
    break;
  case Ease::ElasticEaseOut:
    stepValue = Easing::Elastic::easeOut(time, beginning, change, duration);
    break;
  case Ease::ElasticEaseInOut:
    stepValue = Easing::Elastic::easeInOut(time, beginning, change, duration);
    break;
  case Ease::CubicEaseIn:
    stepValue = Easing::Cubic::easeIn(time, beginning, change, duration);
    break;
  case Ease::CubicEaseOut:
    stepValue = Easing::Cubic::easeOut(time, beginning, change, duration);
    break;
  case Ease::CubicEaseInOut:
    stepValue = Easing::Cubic::easeInOut(time, beginning, change, duration);
    break;
  case Ease::CircEaseIn:
    stepValue = Easing::Circ::easeIn(time, beginning, change, duration);
    break;
  case Ease::CircEaseOut:
    stepValue = Easing::Circ::easeOut(time, beginning, change, duration);
    break;
  case Ease::CircEaseInOut:
    stepValue = Easing::Circ::easeInOut(time, beginning, change, duration);
    break;
  case Ease::BounceEaseIn:
    stepValue = Easing::Bounce::easeIn(time, beginning, change, duration);
    break;
  case Ease::BounceEaseOut:
    stepValue = Easing::Bounce::easeOut(time, beginning, change, duration);
    break;
  case Ease::BounceEaseInOut:
    stepValue = Easing::Bounce::easeInOut(time, beginning, change, duration);
    break;
  case Ease::BackEaseIn:
    stepValue = Easing::Back::easeIn(time, beginning, change, duration);
    break;
  case Ease::BackEaseOut:
    stepValue = Easing::Back::easeOut(time, beginning, change, duration);
    break;
  case Ease::BackEaseInOut:
    stepValue = Easing::Back::easeInOut(time, beginning, change, duration);
    break;
  case Ease::Linear:
  default:
    stepValue = change * time / duration + beginning;
    break;
  }

//  stepValue = ((m_options.stopValue - m_options.startValue) * progress) + m_options.startValue;

  return stepValue;
};

bool Tween::isComplete() {
  return m_isComplete;
};

uint32_t Tween::getStartTime() {
  return m_startTime;
}

uint32_t Tween::getStopTime() {
  return m_stopTime;
}

