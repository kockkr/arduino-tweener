#ifndef kok_Tween_h
#define kok_Tween_h

#include "Arduino.h"
#include "easing/kok_Easing.h"

namespace kok {

  class Tween {

    public: enum Direction {forward, reverse, alternate, alternateReverse};
    public: enum Ease {
      Linear,
      QuadEaseIn, QuadEaseOut, QuadEaseInOut,
      QuartEaseIn, QuartEaseOut, QuartEaseInOut,
      QuintEaseIn, QuintEaseOut, QuintEaseInOut,
      SineEaseIn, SineEaseOut, SineEaseInOut,
      ExpoEaseIn, ExpoEaseOut, ExpoEaseInOut,
      ElasticEaseIn, ElasticEaseOut, ElasticEaseInOut,
      CubicEaseIn, CubicEaseOut, CubicEaseInOut,
      CircEaseIn, CircEaseOut, CircEaseInOut,
      BounceEaseIn, BounceEaseOut, BounceEaseInOut,
      BackEaseIn, BackEaseOut, BackEaseInOut
    };
    private: struct Options {
      uint32_t duration;
      double startValue;
      double stopValue;
      Ease ease;
      int32_t delay;
      uint8_t iterations;
      Direction direction;
    };

    private:
      uint8_t m_uid;
      uint32_t m_currentIteration;

      Options m_options;
      uint32_t m_startTime = 0;
      uint32_t m_stopTime = 0;
      bool m_isComplete = false;

    public:
      uint32_t uid();
      Tween() = default;
      Tween(double startValue, double stopValue, uint32_t duration, Ease ease = Ease::Linear, int32_t delay = 0, uint8_t iterations = 1, Direction direction = Direction::forward);
      void start(uint32_t startTime = NULL);
//      void reset();
//      void restart();
      double step();
      double step(double progress);
      double interpolate(double time, double beginning, double change, double duration);
      bool isComplete();
      uint32_t getStartTime();
      uint32_t getStopTime();

  };
  
};

#endif
