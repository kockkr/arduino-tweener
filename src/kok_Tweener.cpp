#include "kok_Tweener.h"

using namespace kok;

Tweener::Tweener(int32_t iterations) {
  m_iterations = iterations;
}

void Tweener::start() {
  // if queue is empty, return
  if (m_queue.length() == 0) {
    return;
  }

  m_currentTween = m_queue.first();
  m_currentTween.start();
}

void Tweener::pause() {
  // set paused state and record paused time
  m_isPaused = true;
  m_pauseTime = millis();
}

double Tweener::step() {
  static bool queued = false;

  // if no tweens to step through, return
  if (m_queue.length() == 0) {
    queued = false;
    return 0;
  }

  // if first time, get first tween
  if (queued == false) {
    m_currentTween = m_queue.first();
    queued = true;
  }

  // identify the current tween
  if (m_currentTween.isComplete()) {

    // TODO:
    // uint32_t drift = millis() - m_currentTween.getEndTime();

    if (m_currentTween.uid() == m_queue.find(m_queue.length() - 1).uid()) {
      if (m_iterations < 0 || m_curIteration < m_iterations) {
        uint32_t stopTime;

        stopTime = m_currentTween.getStopTime();

        m_curIteration++;
        m_currentTween = m_queue.next();
        // set tween's start time to end time of previous tween to prevent drift
        m_currentTween.start(stopTime);
//        m_currentTween.reset();
      }
    } else {
      uint32_t stopTime;

      stopTime = m_currentTween.getStopTime();
      
      m_currentTween = m_queue.next();
      // set tween's start time to end time of previous tween to prevent drift
      m_currentTween.start(stopTime);
//      m_currentTween.reset();
    }
  }

  return m_currentTween.step();
}

double Tweener::step(double progress) {
  return m_currentTween.step(progress);
}

uint32_t Tweener::push(Tween tween) {
  return m_queue.push(tween);
};

uint32_t Tweener::unshift(Tween tween) {
  return m_queue.unshift(tween);
}
/*
uint32_t Tweener::insertBefore(uint32_t id, Tween tween) {
  return m_queue.insertBefore(id, tween);
}
*/
Tween Tweener::pop() {
  return m_queue.pop();
}

Tween Tweener::shift() {
  return m_queue.shift();
}

/*
uint32_t Tweener::remove(uint32_t id) {
  return m_queue.remove(id);
}
*/
